Para crear un Jar ejecutable debemos de hacer una de las siguientes dos configuraciones en función de si usamos Maven o Gradle:

### Maven
Añadir lo siguiente al pom.xml del proyecto:

```
<plugin>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-maven-plugin</artifactId>
    <configuration>
        <executable>true</executable>
    </configuration>
</plugin>
```

### Gradle

Añadir lo siguiente al build.gradle del proyecto:

Configuración Gradle Jar ejecutableJavaScript

```
bootJar {
    launchScript()
}
```

Arrancar la aplicación desde el Jar, además de soportar las opciones anteriormente mencionadas, implica lo siguiente:

El script inicia la aplicación con el usuario propietario del script
El script hace un seguimiento del PID de la aplicación empleando /var/run/<nombreaplicacion>/<nombreaplicacion>.pid
El script escribe los logs en /var/log/<nombreaplicacion>.log

### Instalación como servicio init.d  (System V)

Para este caso vamos a aprovechar las propiedades anteriormente mencionadas de los Jars ejecutables que se pueden crear en Spring-Boot.

En este caso, la tarea será tan sencilla como añadir un enlace simbólico al Jar en el directorio init.d:

```
sudo ln -s /var/<pathaplicacion>/<nombreaplicacion>.jar /etc/init.d/<nombreaplicacion>
```

Una vez hecho esto, se puede arrancar/parar/reiniciar la aplicación exactamente igual que con cualquier otro servicio:

```
service <nombreaplicacion> start
```

Del mismo modo, podemos marcar la aplicación (servicio) para que se inicie de forma automática al arrancar el equipo:

```
update-rc.d <nombreaplicacion> defaults
```

### Asegurando el servicio

Tal como se ha indicado, el servicio init.d arrancará la aplicación con el usuario propietario de la aplicación. En este sentido, es importante que este usuario disponga de los permisos necesarios para que la aplicación se comporte correctamente.

El usuario empleado para iniciar la aplicación debería de ser uno sin posibilidad de login. Podemos emplear el siguiente comando para modificar el shell y prevenir login al usuario:

```
chsh -s /usr/sbin/nologin <usuario>
```

Podemos cambiar el usuario propietario de la aplicación del siguiente modo:

```
chown <usuario>:<grupo> <nombreaplicacion>.jar
```

El jar de la aplicación debería de tener únicamente propiedades de lectura y ejecución para su propietario, para ello cambiamos sus atributos:

```
chmod 500 <nombreaplicacion>.jar
```

Al final deberíamos tener una estructura como la siguiente:

# <img src="https://blog.marcnuri.com/wp-content/uploads/sites/3/2017/09/propiedades-aplicacion.png" width="100%" height="100%">


### Instalación como servicio systemd

Hoy en día, la mayoría de distribuciones de Linux emplean systemd para gestionar los procesos del sistema. A diferencia de SystemV donde los parámetros de inicio son por convención, en el caso de systemd es por configuración, por lo que para habilitar un servicio es necesario crear un script de configuración.

### Script de configuración

Los script se ubican en el directorio ```/etc/systemd/service```, por lo que para nuestra aplicación tendremos que crear un fichero llamado ```/etc/systemd/service/<nombreaplicacion>.service``` como el siguiente:

```
[Unit]
Description=<nombreaplicacion>
After=syslog.target

[Service]
User=<usuarioejecutor>
ExecStart=/path/a/aplicacion/<nombreaplicacion>.jar
SuccessExitStatus=143

[Install]
WantedBy=multi-user.target
```

Para marcar el servicio para que se inicie de forma automática al arrancar el equipo emplearemos el siguiente comando:

```
sudo systemctl enable nombreaplicacion.service
```

### Referencias

https://docs.spring.io/spring-boot/docs/current-SNAPSHOT/reference/html/deployment.html#deployment



